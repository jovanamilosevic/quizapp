package com.example.quiz

data class Pitanja (
        val id: Int,
        val pitanje: String,
        val slika: Int,
        val opcijaJedan:String,
        val opcijaDva:String,
        val opcijaTri:String,
        val opcijaCetiri:String,
        val ispravanOdgovor: Int
)