package com.example.quiz

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_prvo_pitanje.*
import kotlinx.android.synthetic.main.activity_rezultat.*

class RezultatActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rezultat)

        val ukupnoPitanja=intent.getIntExtra(Konstante.UKUPNO_PITANJA,0)
        val tacniOdgovori=intent.getIntExtra(Konstante.TACAN_ODGOVOR,0)


        tv_rezultat.text="$tacniOdgovori / $ukupnoPitanja"

        btn_kraj.setOnClickListener{
            startActivity(Intent(this,MainActivity::class.java))

        }
    }
}