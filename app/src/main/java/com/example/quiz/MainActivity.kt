@file:Suppress("DEPRECATION")

package com.example.quiz

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        /**Sklonjen vrh ekrana**/
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN

        /*Klik na dugme ZAPOCNI KVIZ*/
        buttonZapocniIgru.setOnClickListener{
        val intent= Intent(this,PitanjaActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}