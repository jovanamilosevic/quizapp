package com.example.quiz

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.widget.TintableCompoundDrawablesView
import kotlinx.android.synthetic.main.activity_prvo_pitanje.*

class PitanjaActivity : AppCompatActivity(), View.OnClickListener {

    private var mTrenutnaPozicija:Int=1
    private var mListaPitanja:ArrayList<Pitanja>?=null
    private  var mSelektovanaOpcija:Int=0
    private var mTacanOdgovor:Int=0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prvo_pitanje)

        mListaPitanja=Konstante.getPitanja();
        setPitanje()

        tv_opcija_jedan.setOnClickListener(this)
        tv_opcija_dva.setOnClickListener(this)
        tv_opcija_tri.setOnClickListener(this)
        tv_opcija_cetiri.setOnClickListener(this)
        btn_potvrdi.setOnClickListener(this)


    }
    //funkcija
    private fun setPitanje(){

        val pitanje= mListaPitanja!![mTrenutnaPozicija-1]

        podrazumevaniPrikazOpcija()

        if(mTrenutnaPozicija==mListaPitanja!!.size){
            btn_potvrdi.text="KRAJ"

        }else{
            btn_potvrdi.text="POTVRDI"
        }

        proggresBar.progress=mTrenutnaPozicija
        tv_proggres.text="$mTrenutnaPozicija"+"/"+proggresBar.max

        tv_pitanje.text=pitanje!!.pitanje
        iv_slika.setImageResource(pitanje.slika)
        tv_opcija_jedan.text=pitanje.opcijaJedan
        tv_opcija_dva.text=pitanje.opcijaDva
        tv_opcija_tri.text=pitanje.opcijaTri
        tv_opcija_cetiri.text=pitanje.opcijaCetiri
    }

    //podrazumevani izgled dugmeta
    private fun podrazumevaniPrikazOpcija(){
        val opcije=ArrayList<TextView>()
        opcije.add(0, tv_opcija_jedan)
        opcije.add(1,tv_opcija_dva)
        opcije.add(2,tv_opcija_tri)
        opcije.add(3,tv_opcija_cetiri)


        for (o in opcije){
            o.setTextColor(Color.parseColor("#7A8089"))
            o.typeface= Typeface.DEFAULT
            o.background=ContextCompat.getDrawable(this,
                    R.drawable.defoult_option_border_bg
            )
        }


    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tv_opcija_jedan -> {
                selektovanaOpcija(tv_opcija_jedan, 1)
            }
            R.id.tv_opcija_dva -> {
                selektovanaOpcija(tv_opcija_dva, 2)
            }
            R.id.tv_opcija_tri -> {
                selektovanaOpcija(tv_opcija_tri, 3)
            }
            R.id.tv_opcija_cetiri -> {
                selektovanaOpcija(tv_opcija_cetiri, 4)
            }
            R.id.btn_potvrdi -> {
                if (mSelektovanaOpcija == 0) {
                    mTrenutnaPozicija++

                    when {
                        mTrenutnaPozicija <= mListaPitanja!!.size -> {
                            setPitanje()
                        }
                        else -> {

                            val intent = Intent(this, RezultatActivity::class.java)
                            intent.putExtra(Konstante.TACAN_ODGOVOR, mTacanOdgovor)
                            intent.putExtra(Konstante.UKUPNO_PITANJA, mListaPitanja!!.size)
                            startActivity(intent)

                        }
                    }
                } else {
                    val pitanje = mListaPitanja?.get(mTrenutnaPozicija - 1)
                    if (pitanje!!.ispravanOdgovor != mSelektovanaOpcija) {
                        odgovor(mSelektovanaOpcija, R.drawable.wrong_option_border_bg)
                    } else {
                        mTacanOdgovor++
                    }
                    odgovor(pitanje.ispravanOdgovor, R.drawable.corect_option_border_bg)


                    if (mTrenutnaPozicija == mListaPitanja!!.size) {
                        btn_potvrdi.text = "KRAJ"
                    } else {
                        btn_potvrdi.text = "SLEDECE PITANJE"
                    }
                    mSelektovanaOpcija = 0
                }
            }

        }
    }

    private fun odgovor(odgovor:Int,drawableView:Int){
        when(odgovor){
            1->{
                tv_opcija_jedan.background=ContextCompat.getDrawable(this, drawableView)
            }
            2->{
                tv_opcija_dva.background=ContextCompat.getDrawable(this, drawableView)
            }
            3->{
                tv_opcija_tri.background=ContextCompat.getDrawable(this, drawableView)
            }
            4->{
                tv_opcija_cetiri.background=ContextCompat.getDrawable(this, drawableView)
            }
        }
    }

    private  fun selektovanaOpcija(tv: TextView, brojSelektovanihOpicija:Int){

        podrazumevaniPrikazOpcija()

        mSelektovanaOpcija=brojSelektovanihOpicija
        tv.setTextColor(Color.parseColor("#616161"))
        tv.setTypeface(tv.typeface,Typeface.BOLD)
        tv.background=ContextCompat.getDrawable(this,
                R.drawable.defoult_option_border_bg
        )
    }
}