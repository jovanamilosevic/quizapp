package com.example.quiz

object Konstante {

        const val UKUPNO_PITANJA:String="ukupno_pitanja"
        const val TACAN_ODGOVOR:String="tacan_odgovor"
    fun getPitanja(): ArrayList<Pitanja>{
        val  listaPitanja=ArrayList<Pitanja>()
        //prvo pitanje

        val pitanje1=Pitanja(
                1,
                "Koloko vremena je potrebno zemlji da se okrene oko svoje ose?",
                R.drawable.pitanje1,
                "12 meseci",
                "24 sata",
                "30 dana",
                "6 meseci",
                2
        )
        listaPitanja.add(pitanje1)


        //drugo pitanje
        val pitanje2=Pitanja(
                2,
                "Koloko dirki ima standardni klavir?",
                R.drawable.pitanje2,
                "76",
                "62",
                "88",
                "90",
                3
        )
        listaPitanja.add(pitanje2)


        //trece pitanje
        val pitanje3=Pitanja(
                3,
                "U kom manastiru se nalazi freska -BELI ANDJEO-? ",
                R.drawable.pitanje3,
                "manastir Tumane",
                "manastir Studenica",
                "manastir Mileseva",
                "manastir Ostrog",
                3
        )
        listaPitanja.add(pitanje3)


        //cetvrto pitanje
        val pitanje4=Pitanja(
                1,
                "Hemijska formula za kuhinjsku so?",
                R.drawable.pitanje4,
                "NaCl",
                "NaOH",
                "KCl",
                "KOH",
                1
        )
        listaPitanja.add(pitanje4)


        //peto pitanje
        val pitanje5=Pitanja(
                1,
                "Koga smatraju osnivačem teorije o evoluciji?",
                R.drawable.pitanje5,
                "Darvin",
                "Dikens",
                "Bajron",
                "Oskar Vajld",
                1
        )
        listaPitanja.add(pitanje5)


        //sesto pitanje
        val pitanje6=Pitanja(
                1,
                "Koja figura je najjača u šahu?",
                R.drawable.pitanje6,
                "top",
                "kralj",
                "konj",
                "kraljica",
                4
        )
        listaPitanja.add(pitanje6)


        //sadmo pitanje
        val pitanje7=Pitanja(
                1,
                "Šta je izumeo Aleksandar Fleming?",
                R.drawable.pitanje7,
                "mikroskop",
                "zakon odrzavanja energije",
                "penicilin",
                "redgenske zrake",
                3
        )
        listaPitanja.add(pitanje7)


        //osmo pitanje
        val pitanje8=Pitanja(
                1,
                "Koliko najmanje gemova čini teniski set?",
                R.drawable.pitanje8,
                "3",
                "5",
                "6",
                "4",
                3
        )
        listaPitanja.add(pitanje8)


        //deveto pitanje
        val pitanje9=Pitanja(
                1,
                "U kojij bajci se princeza ubola na vreteno?",
                R.drawable.pitanje9,
                "Pepeljuga",
                "Uspavana lepotica",
                "Snezana i sedam patuljaka",
                "Lepotica i zver",
                2
        )
        listaPitanja.add(pitanje9)
        //deseto pitanje
        val pitanje10=Pitanja(
                1,
                "Koji od srpskih gradova je nazivan srpskom Atinom?",
                R.drawable.pitanje10,
                "Beograd",
                "Novi Sad",
                "Subotica",
                "Nis",
                2
        )
        listaPitanja.add(pitanje10)





        return listaPitanja
    }
}